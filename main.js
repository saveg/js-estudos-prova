// Tabs Menu
// Caching DOM
const primeira_tab = document.querySelector('.tab-01');
const segunda_tab = document.querySelector('.tab-02');
const terceira_tab = document.querySelector('.tab-03');

const primeiro_content = document.querySelector('.content-01');
const segundo_content = document.querySelector('.content-02');
const terceiro_content = document.querySelector('.content-03');

function resetTabs(){

    primeiro_content.style.display = 'none';
    segundo_content.style.display = 'none';
    terceiro_content.style.display = 'none';


}

primeira_tab.addEventListener('click', () =>{

    resetTabs();
    primeiro_content.style.display = 'block';

});

segunda_tab.addEventListener('click', () =>{

    resetTabs();
    segundo_content.style.display = 'block';

});

terceira_tab.addEventListener('click', () =>{

    resetTabs();
    terceiro_content.style.display = 'block';

});

// Tabs Menu Beautified Code
// Caching DOM
const tabs_wrapper = document.querySelectorAll('.tab');
const content_wrapper = document.querySelectorAll('.content2');

function vanish(){

    content_wrapper.forEach((content) => {
        content.style.display = 'none';
    })

}

tabs_wrapper.forEach((tab) => {

    tab.addEventListener('click', () => {
        vanish();
        var tab_search_id = tab.id.replace('tab-', '#content-');
        var content_show = document.querySelector(tab_search_id);
        content_show.style.display = 'block';
    });

})

// Input(Digitando...) = Input(Digitando...)
// Caching DOM
const input_first_field = document.querySelector('#input-01');
const input_second_field = document.querySelector('#input-02');

input_first_field.addEventListener('keyup', () =>{

    input_second_field.value = input_first_field.value;

})

// Form Validation
// Caching DOM
const name_input = document.querySelector('#name');
const email_input = document.querySelector('#email');
const send_button = document.querySelector('#send-button');
const error_span = document.querySelector('.error');
const cpf_button = document.querySelector('#cpf-button');
const cpf_field = document.querySelector('#cpf');
const select_field = document.querySelector('#estados');


send_button.addEventListener('click', (clicked) => {

    
    if(name_input.value == '' || email_input.value == ''){
        clicked.preventDefault();
        error_span.innerText = 'Fields empty';
    }else{
        clicked.preventDefault();
        var selectedEstado = select_field.options[select_field.selectedIndex].text; 

        if(selectedEstado == 'Selecione'){
            error_span.innerText = 'Estado inválido'
        }else{
            error_span.style.color = '#4382ac';
            error_span.innerText = `Seu estado é: ${selectedEstado}`;
        }
        
    }

})

cpf_field.disabled = true;

cpf_button.addEventListener('click', () => {

    if (cpf_field.disabled == true){
        cpf_field.disabled = false;
        cpf_button.checked = true;
    }else{
        cpf_field.disabled = true;
        cpf_button.checked = false;
    }

})

// Page with JS -> Appending Elements
// Cachind Elements
const section_container = document.querySelector('#landing-page-container');
const page_container = document.createElement('section');
const page_h1 = document.createElement('h1');

const page_div_text = document.createElement('div');
const page_div_text_p = document.createElement('p');
const page_div_cards = document.createElement('div');

const page_div_cards_card = document.createElement('div');
const page_div_cards_card_p = document.createElement('p') 
const page_div_cards_card_button = document.createElement('button')

const page_div_cards_card2 = document.createElement('div');
const page_div_cards_card2_p = document.createElement('p') 
const page_div_cards_card2_button = document.createElement('button')

page_container.classList.add('page');
section_container.appendChild(page_container);

page_h1.innerText = 'Welcome';
page_container.appendChild(page_h1);

page_div_text.classList.add('text');
page_div_text_p.innerText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae consequat turpis.';
page_div_text.appendChild(page_div_text_p);
page_container.appendChild(page_div_text);

page_div_cards.classList.add('cards');
page_div_cards_card.classList.add('card');
page_div_cards_card_button.classList.add('btn');

page_div_cards_card_p.innerText = 'In sagittis varius egestas. Nunc ut massa sapien. Nunc fermentum neque porttitor magna vehicula.';
page_div_cards_card_button.innerText = 'See more';

page_div_cards_card.appendChild(page_div_cards_card_p);
page_div_cards_card.appendChild(page_div_cards_card_button);

page_div_cards_card2.classList.add('card');
page_div_cards_card2_button.classList.add('btn');

page_div_cards_card2_p.innerText = 'Pellentesque hendrerit, mi non luctus auctor, nisi augue tempus purus, sed auctor nisl ligula vitae leo.';
page_div_cards_card2_button.innerText = 'See more';

page_div_cards_card2.appendChild(page_div_cards_card2_p);
page_div_cards_card2.appendChild(page_div_cards_card2_button);
page_div_cards.appendChild(page_div_cards_card2);

page_div_cards.appendChild(page_div_cards_card);
page_div_cards.appendChild(page_div_cards_card2);

page_container.appendChild(page_div_cards);

// Moving the ghost around
// Caching ghost
const ghost = document.querySelector('#ghost');

// Keycodes: W = 87, A = 65, S = 83, D = 68
document.addEventListener('keydown', (key) => {

    if(key.keyCode == 87){
        var current_position = ghost.style.top;
        current_position = current_position.replace('px', '');
        current_position = parseInt(current_position) - 10;
        ghost.style.top = `${current_position}px`;
        console.log(ghost.style.top);
    }else{

        if(key.keyCode == 65){
            var current_position = ghost.style.left;
            current_position = current_position.replace('px', '');
            current_position = parseInt(current_position) + 10;
            ghost.style.top = `${current_position}px`;
            console.log(ghost.style.top);
        }else{

            if(key.keyCode == 83){
                var current_position = ghost.style.top;
                current_position = current_position.replace('px', '');
                current_position = parseInt(current_position) + 10;
                ghost.style.top = `${current_position}px`;
                console.log(ghost.style.top);
            }else{

                if(key.keyCode == 68){
                    var current_position = ghost.style.left;
                    current_position = current_position.replace('px', '');
                    current_position = parseInt(current_position) - 10;
                    ghost.style.top = `${current_position}px`;
                    console.log(ghost.style.top);
                }
            }
        }
    }
})
